package com.niemiro.projects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Entity(name="project")
@AllArgsConstructor
@NoArgsConstructor
class Project
{
    @Id
    Long id;

    @Column(name = "name")
    String name;
}
