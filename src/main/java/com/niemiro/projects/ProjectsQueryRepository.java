package com.niemiro.projects;

import com.niemiro.projects.dto.ProjectDto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

interface ProjectsQueryRepository extends CrudRepository<Project, Long>
{
    @Query("select new com.niemiro.projects.dto.ProjectDto(id, name) from project")
    List<ProjectDto> getProjects();
}
