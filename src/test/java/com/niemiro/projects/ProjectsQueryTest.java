package com.niemiro.projects;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
class ProjectsQueryTest extends IntegrationTest
{
    @Autowired
    ProjectsQueryRepository repository;

    @Test
    public void test()
    {
        Project testProject = new Project(1L, "Test project 1");
        repository.save(testProject);

        repository.getProjects()
            .forEach(project -> log.info(project.toString()));
    }
}
