# Getting Started
This application is an example how to use postgresql testcontainers in JUnit tests.
It includes:
* spring-data
* postgres testcontainers
* JUnit tests only

# Running the application
1. Install and run Docker
2. Run ProjectsQueryTest as JUnit test (or run maven install)
